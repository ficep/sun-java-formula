{%- from 'sun-java/settings.sls' import java with context %}

{#- require a source_url - there is no default download location for a jdk #}

{%- if java.source_url is defined %}

  {%- set tarball_file = java.prefix + '/' + java.source_url.split('/') | last %}

curl:
    pkg.installed

java-install-dir:
  file.directory:
    - name: {{ java.prefix }}
    - user: root
    - group: root
    - mode: 755
    - makedirs: True

download-jdk-tarball:
  cmd.run:
    - name: curl {{ java.dl_opts }} -o '{{ tarball_file }}' '{{ java.source_url }}'
    - unless: test -d {{ java.java_real_home }} || test -f {{ tarball_file }}
    - require:
      - file: java-install-dir
      - pkg: curl

unpack-jdk-tarball:
  archive.extracted:
    - name: {{ java.prefix }}
    - source: file://{{ tarball_file }}
    {%- if java.source_hash %}
    - source_hash: sha256={{ java.source_hash }}
    {%- endif %}
    - archive_format: tar
    - options: z
    - user: root
    - group: root
    - if_missing: {{ java.java_real_home }}
    - onchanges:
      - cmd: download-jdk-tarball

remove-jdk-tarball:
  file.absent:
    - name: {{ tarball_file }}

{%- endif %}
